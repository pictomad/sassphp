# README #

Very simple usage - current sassphp compiler "leafo/scssphp": "0.6.6"

# index.php

link css files from server

```
<link rel="stylesheet" type="text/css" href="style.php/style.scss">
<link rel="stylesheet" type="text/css" href="style.php/style2.scss">
```

# style.php

Depends on the required scss file, add some vars

```
require_once "scssphp/scss.inc.php";
use Leafo\ScssPhp\Server;
use Leafo\ScssPhp\Compiler;

$scss = new Compiler();
// switch vars depends on the file
$vars = array('color1' => 'red','color2' => 'orange');

$scss->setVariables($vars);

$directory = "stylesheets";

$server = new Server('stylesheets', null, $scss);
$server->serve();

```

# style2.scss

```
/* style2.scss */
body{
	h1{
		font-size: $fontSize;
		&:hover{
			font-size: $fontSizeHover;
		}
	}
}
```

## external library ##
http://leafo.github.io/scssphp/docs/