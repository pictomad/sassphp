<?php
// SASS PHP Compiler
require_once "scssphp/scss.inc.php";
use Leafo\ScssPhp\Server;
use Leafo\ScssPhp\Compiler;

$scss = new Compiler();
// Useful for debug
//$scss->setLineNumberStyle(Compiler::LINE_COMMENTS);
$scss->setFormatter('Leafo\ScssPhp\Formatter\Expanded');

$fileName = $_SERVER['PATH_INFO'];
$fileName = str_replace('/', '', $fileName);

$vars = array();
switch ($fileName) {
	case 'style.scss':
		$vars = array(
		    'color1' => 'red',
		    'color2' => 'orange',
		);	
		break;
	case 'style2.scss':
		$vars = array(
		    'fontSize' => '30px',
		    'fontSizeHover' => '40px',
		);
		break;
	default:
		// no vars needed
		break;
}

$scss->setVariables($vars);

$directory = "stylesheets";

$server = new Server('stylesheets', null, $scss);
$server->serve();

?>